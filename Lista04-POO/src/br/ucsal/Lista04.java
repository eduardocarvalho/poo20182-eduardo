package br.ucsal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Lista04 {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) throws ParseException {
		System.out.println("O que deseja fazer? \n [1] Incluir \n [2] Excluir \n [3] Listar \n [4] Pesquisar por nome ");
		int opcao = sc.nextInt();


		switch (opcao) {
		case 1:
			System.out.println("Quantos contatos deseja incluir? ");
			int incluir = sc.nextInt();
			String date = "";
			Contato contato = new Contato();
			contato.nome = new String [incluir];
			contato.telefone = new String [incluir];
			contato.anoNascimento = new Integer [incluir];
			contato.dataNascimento = new Date [incluir];


			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");  

			int cont = 0;

			for (int i = 0; cont < incluir; i++) {
				System.out.println("Qual o nome do contato "+(i+1)+"?");
				contato.nome[i] = sc.next();

				System.out.println("Qual o telefone do contato "+(i+1)+"?");
				contato.telefone[i] = sc.next();

				System.out.println("Qual o ano de nascimento do contato "+(i+1)+"?");
				contato.anoNascimento[i] = sc.nextInt();

				System.out.println("Qual a data de nascimento do contato "+(1)+"? [FORMATO: ''##/##/####'']");
				date = sc.next();
				df = new SimpleDateFormat("dd/MM/yyyy");  
				contato.dataNascimento[i] = df.parse(date);

				cont++;
			}

			break;

		default:
			break;
		}

	}

}class Contato{
	String [] nome;
	String []telefone;
	Integer []anoNascimento;
	Date [] dataNascimento;
}
