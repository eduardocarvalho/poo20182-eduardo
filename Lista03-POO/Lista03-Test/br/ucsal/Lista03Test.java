package br.ucsal;

import org.junit.Test;

import junit.framework.Assert;

public class Lista03Test {
	@Test
	public void verificarMetodo() {

		int [] vet = { 4,8,7,4,3};

		int valorEsperado = 8;

		int valorReal = Lista03.encontrarMaiorNumero(vet);

       Assert.assertEquals(valorEsperado, valorReal);


	}
}
